package echogorm

import (
	"fmt"
	"net/http"

	_ "github.com/go-sql-driver/mysql"
	"github.com/jinzhu/gorm"
	"github.com/labstack/echo"
)

var db *gorm.DB

// GORMConfig GORMConfig
type GORMConfig struct {
	Host     string
	Port     int
	Username string
	Password string
	Database string
	Charset  string
}

// GORMContext GORMContext
type GORMContext struct {
	echo.Context
}

// DB DB
func (c *GORMContext) DB() *gorm.DB {
	return db
}

// GORMWithConfig dd
func GORMWithConfig(config GORMConfig) echo.MiddlewareFunc {
	return func(next echo.HandlerFunc) echo.HandlerFunc {
		return func(c echo.Context) error {
			var err error
			db, err = gorm.Open("mysql", fmt.Sprintf(
				"%s:%s@(%s:%d)/%s?charset=%s&parseTime=True",
				config.Username,
				config.Password,
				config.Host,
				config.Port,
				config.Database,
				config.Charset,
			))
			defer db.Close()
			if err != nil {
				return c.String(http.StatusInternalServerError, "服务出错")
			}

			cc := &GORMContext{c}
			return next(cc)
		}
	}
}
